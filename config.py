import os
import click


class Config(object):
    ENV = os.environ["ENV"] if "ENV" in os.environ else "DEVELOPMENT"
    CSRF_ENABLED = True
    SECRET_KEY = "b'\xd3b1\xb7\x11\xeaYB\xd1Pve!\xa18\x06\xa3\xb0N\xfb\x89\x99o\xfc'"
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = (
        "postgresql+psycopg2://"
        + os.environ["DB_USERNAME"]
        + ":"
        + os.environ["DB_PASSWORD"]
        + "@"
        + os.environ["DB_HOST"]
        + ":"
        + os.environ["DB_PORT"]
        + "/"
        + os.environ["DB_DATABASE"]
    )
    click.echo(SQLALCHEMY_DATABASE_URI)


class TestingConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = (
        "postgresql+psycopg2://"
        + os.environ["DB_USERNAME"]
        + ":"
        + os.environ["DB_PASSWORD"]
        + "@"
        + os.environ["DB_HOST"]
        + ":"
        + os.environ["DB_PORT"]
        + "/"
        + os.environ["DB_DATABASE"]
    )
