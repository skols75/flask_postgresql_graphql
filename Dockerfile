# Official Python image
FROM python:3.8.5
# Work Directory
WORKDIR /usr/src/app
# Copy requirements.txt
COPY ./requirements.txt /usr/src/app/requirements.txt
# Install dependecies
RUN pip install -r requirements.txt
# Copy project code to the work directory
COPY . /usr/src/app
EXPOSE 5000
# Run the application
CMD [ "python", "manage.py", "runserver" ]
