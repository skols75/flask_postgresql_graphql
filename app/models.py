from app import db
from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4


class User(db.Model):
    __tablename__ = "users"
    user_uuid = db.Column(
        UUID(as_uuid=True),
        primary_key=True,
        unique=True,
        nullable=False,
        default=uuid4,
    )
    username = db.Column(db.String(256), index=True, unique=True)
    name = db.Column(db.String(256))
    last_name = db.Column(db.String(256))
    email = db.Column(db.String(256))
    profile_id = db.Column(UUID(as_uuid=True), db.ForeignKey("profiles.profile_uuid"))
    profile = db.relationship("Profile")

    def __repr__(self):
        return f"<User {self.username}>"


class Profile(db.Model):
    __tablename__ = "profiles"
    profile_uuid = db.Column(
        UUID(as_uuid=True),
        primary_key=True,
        unique=True,
        nullable=False,
        default=uuid4,
    )
    role = db.Column(db.String(256))
    description = db.Column(db.Text)
    skills = db.relationship("Skill")

    def __repr__(self):
        return f"<Profile {self.role}>"


class Skill(db.Model):
    __tablename__ = "skills"
    skills_uuid = db.Column(
        UUID(as_uuid=True),
        primary_key=True,
        unique=True,
        nullable=False,
        default=uuid4,
    )
    name = db.Column(db.String(256))
    score = db.Column(db.Integer)
    profile_uuid = db.Column(UUID(as_uuid=True), db.ForeignKey("profiles.profile_uuid"))

    def __repr__(self):
        return f"<Skill {self.name} with score {self.score}>"
