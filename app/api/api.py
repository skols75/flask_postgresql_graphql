"""
api.py
- provides the API endpoints for consuming and producing
  REST requests and responses
"""

from flask import Blueprint, jsonify, request
from .models import User

api = Blueprint("api", __name__)

# Add user
@api.route("/api/user/", methods=["POST"])
def add_user():

    return
